---
title: Robin's Visualization
subtitle: Inspired by a really cool guy who made a really nice piano part in his song.
comments: false
---

I tried to focused the *Visualization* on Animals that I really like. But with only 6 classes the video was too boring in my opinion, so I added a few more random classes to spice it um. And the results are as follows.

I hope you like it, I tried to make a video which won't cause headache or eye cancer. :relieved:

### The command I used to create this video: 

```
$ python visualize.py --song ./wav/input.wav --num_classes 10 --classes 18 269 270 294 295 296 451 567 687 948 --jitter 0.9 --truncation 0.6 --resolution 256

```

{{< video robin >}}


