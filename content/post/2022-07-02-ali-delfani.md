---
title: Ali's Visualization
subtitle: Inspired by my goodbye party
comments: false
---

My close friends had thrown a goodbye party for me before I traveled abraod for my studies back in 2019. **Never will I** forget that day; this song is evocative of that party. Most of the people attended the party are now living in different countries.  
I've tried different options to create a video in which _visualization_ isn't monotonous.

### Competition Entry:

```
$ python visualize.py --song mine.wav --resolution 128 --duration 28 --pitch_sensitivity 260 --tempo_sensitivity 0.2 --depth 0.4 --num_classes 8 --classes 34 72 110 296 470 669 808 998 --jitter 0.9 --truncation 0.8 --smooth_factor 25 --batch_size 3 --output_file ali.mp4


```

{{< video ali >}}